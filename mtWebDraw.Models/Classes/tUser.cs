﻿using System;

namespace mtWebDraw.Models
{
    /// <summary>
    /// 用户信息表
    /// </summary>
    public class tUser
    {
    
        /// <summary>
        /// 标识
        /// </summary>
        public virtual Guid ID { get; set; }
        
        /// <summary>
        /// 用户名
        /// </summary>
        public virtual String UserName { get; set; }
        
        /// <summary>
        /// 密码
        /// </summary>
        public virtual String Password { get; set; }
        
        /// <summary>
        /// 是否为管理员
        /// </summary>
        public virtual Boolean IsAdmin { get; set; }
        
        /// <summary>
        /// 所属部门
        /// </summary>
        public virtual Guid DepartmentID { get; set; }
        
        /// <summary>
        /// 姓名
        /// </summary>
        public virtual String FullName { get; set; }
        
        /// <summary>
        /// 邮箱
        /// </summary>
        public virtual String Email { get; set; }
        
        /// <summary>
        /// 手机
        /// </summary>
        public virtual String Mobile { get; set; }

        /// <summary>
        /// 授权码
        /// </summary>
        public virtual String AuthCode { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public virtual DateTime CreateTime { get; set; }
        
        /// <summary>
        /// 是否停用
        /// </summary>
        public virtual Boolean IsStop { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public virtual String Remark { get; set; }
        
    }
}