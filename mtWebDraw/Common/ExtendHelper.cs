﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using mtTools;

namespace mtWebDraw.Common
{
    /// <summary>
    /// 静态扩展操作
    /// </summary>
    public static class ExtendHelper
    {

        /// <summary>
        /// 返回ExtJS的复选框选中状态
        /// </summary>
        /// <param name="ckeckedName"></param>
        /// <returns></returns>
        public static bool IsExtChecked(this string ckeckedName)
        {
            return ckeckedName.IsNullOrWhiteSpace() ? false : (ckeckedName == "on" ? true : false);
        }

        /// <summary>
        /// 判断用户是否有此流程图的大于等于该操作权限
        /// </summary>
        /// <param name="sUser"></param>
        /// <param name="ChartID"></param>
        /// <param name="Code"></param>
        /// <returns></returns>
        public static bool IsOperChart(this User sUser, Guid ChartID, byte Code)
        {
            if (sUser.UserName == CacheData.sysadmin)
                return true;

            var chart = CacheData.GetChart(ChartID);
            if (chart.Owner == sUser.ID)
                return true;

            var clist = CacheData.GetUserChartCodeList(sUser.ID);
            var c = clist.Where(p => p.UserID == sUser.ID && p.ChartID == ChartID).FirstOrDefault();
            if (c != null && c.Code >= Code)
                return true;

            return false;
        }

    }
}